from player import Player
from monster import Monster, Mummy, Alien #from mon fichier importe ma classe
from sounds import SoundManager
from comete_event import CometFallEvent
import pygame
# Création d'une 2dne classe qui va représenter le jeu


class Game:

    def __init__(self):
        # Définir si le jeu à commencé ou non
        self.is_playing = False
        # générer notre joueur
        self.all_players = pygame.sprite.Group()
        self.player = Player(self)
        self.all_players.add(self.player)
        #Générer le manager de la pluie de comète
        self.comete_event = CometFallEvent(self)
        # groupe de monstre
        self.all_monsters = pygame.sprite.Group()
        #Gérer le son
        self.sound_manager = SoundManager()
        #mettre le score à 0
        self.font = pygame.font.Font("assets/my_font.ttf", 25)
        self.score = 0
        self.pressed = {}
      

    def start(self):
        self.is_playing = True
        # Pour que le spawn des monstres démarre en meme temps que le jeu
        self.spawn_monster(Mummy)
        self.spawn_monster(Mummy)
        self.spawn_monster(Alien)

    def add_score(self, points=10):
        self.score += points

    def game_over(self):
        #Remettre le jeu à 0
        self.all_monsters = pygame.sprite.Group()
        self.comete_event.all_comets = pygame.sprite.Group()
        self.player.health = self.player.max_health
        self.comete_event.reset_percent()
        self.is_playing = False
        self.score = 0
        #jouer le son
        self.sound_manager.play('game_over')

    def update(self, screen):
        #Afficher le score sur l'écran
        score_text = self.font.render(f"Score : {self.score}", 1, (0, 0, 0))
        screen.blit(score_text, (20, 20))

        # appliquer l'image du joueur
        screen.blit(self.player.image, self.player.rect)

        # Actualiser la barre de vie du joueur
        self.player.update_health_bar(screen)

        #Actualiser la barre d'évènement du jeu
        self.comete_event.update_bar(screen)

        #Mettre à jour l'animation du joueur
        self.player.update_animation()

        # Récupérer les projectiles du joueur
        for projectile in self.player.all_projectiles:
            projectile.move()

        # Récupérer les monstres
        for monster in self.all_monsters:
            monster.forward()
            monster.update_health_bar(screen)
            monster.update_animation()

        #Récupérer les comètes
        for comet in self.comete_event.all_comets:
            comet.fall()

        # Appliquer l'ensemble des images de mon groupe de projectile
        self.player.all_projectiles.draw(screen)

        # Afficher les monstres
        self.all_monsters.draw(screen)

        #Appliquer l'ensemble des images du groupe Comet
        self.comete_event.all_comets.draw(screen)

        # Vérifier si le joueur souhaite aller à gauche ou à droite
        # le + est pour rajouter la largeur du joueur dans le calcul
        if self.pressed.get(pygame.K_RIGHT) and self.player.rect.x + self.player.rect.width < screen.get_width():
            self.player.move_right()
        elif self.pressed.get(pygame.K_LEFT) and self.player.rect.x > 0:
            self.player.move_left()
        print(self.player.rect.x)

    def check_collision(self, sprite, group):
        return pygame.sprite.spritecollide(sprite, group, False, pygame.sprite.collide_mask)

    def spawn_monster(self, monster_class_name):
        self.all_monsters.add(monster_class_name.__call__(self))
