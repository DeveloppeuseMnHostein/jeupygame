import pygame
from projectile import Projectile
import animation


#Creation d'une première classe qui va représenter le joueur
class Player(animation.AnimateSprite):

    def __init__(self, game): #self pour récupérer l'objet courant
        super().__init__('player') #pour récupérer les éléments de la super classe dont la classe hérite
        self.game = game
        self.health = 100 #valeur variable (qui va diminuer au fil des attaques)
        self.max_health = 100 #valeur fixe des pv
        self.attack = 10
        self.velocity = 5 #on peut changer cette valeur pour accélerer ou décellérer
        self.all_projectiles = pygame.sprite.Group()
        #self.image = pygame.image.load('assets/player.png')
        self.rect = self.image.get_rect() #on positionne le joueur pour pouvoir le déplacer
        self.rect.x = 400
        self.rect.y = 500 #en partant du haut de l'écran

    def damage(self, amount):
        if self.health - amount > amount:
            self.health -= amount
        else:
            #si le joueur est mort
            self.game.game_over()

    def update_animation(self):
        self.animate()

    def update_health_bar(self, surface):
        #Dessiner la barre de vie du joueur
        pygame.draw.rect(surface, (60, 63, 60), [self.rect.x + 50, self.rect.y + 20, self.max_health, 7])
        pygame.draw.rect(surface, (111, 210, 46), [self.rect.x + 50, self.rect.y + 20, self.health, 7])

    def launch_projectile(self):
        #créer une nouvelle instance de la classe projectile
        self.all_projectiles.add(Projectile(self))
        #Démarrer l'animation du lancement du projectile
        self.start_animation()
        #jouer le son
        self.game.sound_manager.play('tir')

    def move_right(self):
        #Si le joueur n'est pas en collision avec une entité monstre
        if not self.game.check_collision(self, self.game.all_monsters):
            self.rect.x += self.velocity

    def move_left(self):
        self.rect.x -= self.velocity