import pygame
import random

#Créer une classe pour gérer la comète
class Comet(pygame.sprite.Sprite):

    def __init__(self, comete_event):
        super().__init__()
        #Définir l'image associé à la comète
        self.image = pygame.image.load('assets/comet.png')
        self.rect = self.image.get_rect()
        self.velocity = random.randint(1, 3)
        self.rect.x = random.randint(20, 800)
        self.rect.y = - random.randint(0, 800)
        self.comete_event = comete_event

    def remove(self):
        self.comete_event.all_comets.remove(self)
        self.comete_event.game.sound_manager.play('meteorite')

        #Si il n'y a plus de boule de feu sur le jeu
        if len(self.comete_event.all_comets) == 0:
            print("L'évènement est fini !")
            #Remettre la jauge au départ
            self.comete_event.reset_percent()
            #Refaire apparaitre les monstres
            self.comete_event.fall_mode = False
            self.comete_event.game.start()
            

    def fall(self):
        self.rect.y += self.velocity

        #Si elle ne tombe pas sur le sol
        if self.rect.y >= 550:
            print('sol')
            #Retirer la boule de feu
            self.remove()


        #Vérifier si la boule de feu touche le joueur
        if self.comete_event.game.check_collision(self, self.comete_event.game.all_players):
            print("Joueur touché")
            #Retirer la boule de feu
            self.remove()
            #subir 20 points de dégats
            self.comete_event.game.player.damage(20)