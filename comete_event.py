import pygame
from comet import Comet

#Créer une classe pour gérer cet évènement à un intervalle régulier
class CometFallEvent:

    #Lors du chargement on crée un compteur qui va controler le pourcentage actuel de cette barre
    def __init__(self, game):
        self.percent = 0
        self.percent_speed = 5
        self.game = game
        self.fall_mode = False

        #Définir un groupe de sprite pour stocker les comètes
        self.all_comets = pygame.sprite.Group()

    
    def add_percent(self):
        self.percent += self.percent_speed / 100

    def is_full_loaded(self):
        return self.percent >= 100

    def reset_percent(self):
        self.percent = 0

    def meteor_fall(self):
        #boucle pour les valeurs entre 1 et 10
        for i in range(1, 10):
            #apparaitre une 1ère boule de feu
            self.all_comets.add(Comet(self))

    def attempt_fall(self):
        #Si la jauge d'évènement est totalement chargée
        if self.is_full_loaded() and len(self.game.all_monsters) == 0:
            print("Attention pluie de comètes !")
            self.meteor_fall()
            self.fall_mode = True #Pour activer l'évènement


    def update_bar(self, surface):

        #Ajouter du pourcentage à la barre
        self.add_percent()

        #barre noire en arrière plan
        pygame.draw.rect(surface, (0, 0, 0), [
            0, #axe des x
            surface.get_height() - 20, #récupère la hauteur de la fenetre
            surface.get_width(), #longueur de la fenetre
            10 #épaisseur de la barre
        ])
        #barre rouge de la jauge d'évènement qui s'actualise au fur et à mesure
        pygame.draw.rect(surface, (187, 11, 11), [
            0, #axe des x
            surface.get_height() - 20, #récupère la hauteur de la fenetre
            (surface.get_width() / 100) * self.percent, #longueur de la fenetre
            10 #épaisseur de la barre
        ])