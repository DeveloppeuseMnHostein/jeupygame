import pygame
import random
import animation


#Définition classe des monstres
class Monster(animation.AnimateSprite):

    def __init__(self, game, name, size, offset=0):
        super().__init__(name, size) #on appelle la classe hérité de la classe Animation
        self.game = game
        self.health = 100
        self.max_health = 100
        self.attack = 0.3
        #self.image = pygame.image.load('assets/mummy.png') => on enlève cette ligne puisqu'on utilise la classe animation
        self.rect = self.image.get_rect()
        self.rect.x = 1000 + random.randint(0, 300)
        self.rect.y = 540 - offset
        self.loot_amount = 10
        self.start_animation()

    def set_speed(self, speed):
        self.default_speed = speed
        self.velocity = random.randint(1, 2)

    def set_loot_amount(self, amount):
        self.loot_amount = amount

    def damage(self, amount):
        #Infliger les dégats
        self.health -= amount

        #Vérifier s'il n'est pas mort
        if self.health <= 0 :
            #Re apparaitre comme un nouveau monstre
            self.rect.x = 1000 + random.randint(0, 300)
            self.velocity = random.randint(1, self.default_speed)
            self.health = self.max_health
            #Ajouter les points
            self.game.add_score(self.loot_amount)

            #Si la barre d'évènement est chargée à son maximum
            if self.game.comete_event.is_full_loaded():
                #Retirer les monstres du jeu
                self.game.all_monsters.remove(self)

                #Appel de la méthode pour déclencher la pluie de comète
                self.game.comete_event.attempt_fall()

    def update_animation(self):
        self.animate(loop=True)

    def update_health_bar(self, surface):
        #Définir la couleur de la jauge de vie (vert clair) pour alléger les instructions on met les couleur directement dans la def de la barre de vie
        #bar_color = (111, 210, 46)
        #Définir l'arrière plan de la jauge
        #back_bar_color = (60, 63, 60)

        #Définir la position de la jauge de vie et sa taille
        #bar_position = [self.rect.x + 10, self.rect.y - 20, self.health, 5] => pour alléger le code, on annule ces déclarationds de variable qui ne servent qu'une fois
        #Définir la position de l'arrière plan de la jauge de vie            => et on les reporte directement dans la définition de la barre de vie
        #back_bar_position = [self.rect.x + 10, self.rect.y - 20, self.max_health, 5]

        #Dessiner la barre de vie
        #pygame.draw.rect(surface, (60, 63, 60), back_bar_position)
        pygame.draw.rect(surface, (60, 63, 60), [self.rect.x + 10, self.rect.y - 20, self.max_health, 5])
        pygame.draw.rect(surface, (111, 210, 46), [self.rect.x + 10, self.rect.y - 20, self.health, 5])

    def forward(self):
        #Prévoir la collision avec le group de joueur
        if not self.game.check_collision(self, self.game.all_players):
            self.rect.x -= self.velocity
        #si le monstre est en collision avec le joueur
        else:
            #infliger les dégats au joueur
            self.game.player.damage(self.attack)    


#Définir une classe pour la mummy
class Mummy(Monster):

    def __init__(self, game):
        super().__init__(game, "mummy", (130, 130))
        self.set_speed(3)
        self.set_loot_amount(20)

#Définir une classe pour l'alien
class Alien(Monster):

    def __init__(self, game):
        super().__init__(game, "alien", (300, 300), 130)
        self.health = 250
        self.max_health = 250
        self.attack = 0.8
        self.set_speed(1)
        self.set_loot_amount(40)

