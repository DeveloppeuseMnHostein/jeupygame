import pygame
import math
from game import Game
from player import Player
pygame.init()

#Définir une clock
clock = pygame.time.Clock()
FPS = 60


# Générer la fenetre du jeu et donner un titre au jeu
pygame.display.set_caption("Comète Fall Game")
# definit la taille de la fenetre
screen = pygame.display.set_mode((1080, 720))

# Mettre le background
background = pygame.image.load('assets/bg.jpg')

#Importer et charger la bannière du jeu (écran d'accueil)
banner = pygame.image.load('assets/banner.png')
banner = pygame.transform.scale(banner, (500, 500))
banner_rect = banner.get_rect()
banner_rect.x = math.ceil(screen.get_width() / 4)

#Importer et charger le bouton qui lance la partie
play_button = pygame.image.load('assets/button.png')  
play_button = pygame.transform.scale(play_button, (400, 150))
play_button_rect = play_button.get_rect()
play_button_rect.x = math.ceil(screen.get_width() / 3.33)
play_button_rect.y = math.ceil(screen.get_height() / 2)

# charger le jeu
game = Game()


# Installer le joueur dans l'écran
player = Player(game)

# Pour dire si la fenetre est en cours d'éxécution
running = True

# Boucle tant que la condition est vraie
while running:

    # appliquer l'arrière plan du jeu
    screen.blit(background, (0, -200))

    #Vérifier si le jeu a commencé ou non
    if game.is_playing:
        #déclencher les instructions de la partie
        game.update(screen)
    else:
        #Ajouter l'écran de bienvenue
        screen.blit(play_button, play_button_rect)
        screen.blit(banner, banner_rect)

    # mettre à jour l'écran
    pygame.display.flip()

    # si le joueur ferme la fenetre
    for event in pygame.event.get():
        # on vérifie que l'évènement correspond à la fermeture de la fenetre
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
            print("Fermeture du jeu")
        # détecter si un joueur lache une touche du clavier
        elif event.type == pygame.KEYDOWN:
            game.pressed[event.key] = True

            # Détecter si la touche espace est enclenchée pour lancer le projectile
            if event.key == pygame.K_SPACE:
                if game.is_playing:
                    game.player.launch_projectile()
                else:
                    #Mettre le jeu en mode lancé
                    game.start()
                    #Jouer le son de lancement
                    game.sound_manager.play('click')

        elif event.type == pygame.KEYUP:
            game.pressed[event.key] = False

        elif event.type == pygame.MOUSEBUTTONDOWN:
            #Vérification pour savoir si la souris est en collision avec le bouton jouer
            if play_button_rect.collidepoint(event.pos):
                #Mettre le jeu en mode lancé
                game.start()
                #Jouer le son de lancement
                game.sound_manager.play('click')

    #Fixer le nombre de fps sur ma clock
    clock.tick(FPS)

