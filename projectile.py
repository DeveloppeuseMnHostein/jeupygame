import pygame

#Définir la classe qui va gérer le projectile du joueur
class Projectile(pygame.sprite.Sprite):

    #Définir le constructeur de la classe
    def __init__(self, player):
        super().__init__()
        self.velocity = 4
        self.player = player
        self.image = pygame.image.load('assets/projectile.png')
        self.image = pygame.transform.scale(self.image, (50, 50)) #1er paramètre, objet sur lequel on veut intervenir, 2ème paramètre (taille H, taille W)
        self.rect = self.image.get_rect()
        self.rect.x = player.rect.x + 120
        self.rect.y = player.rect.y + 80
        self.origin_image = self.image #pour garder une base de l'image
        self.angle = 0 #valeur de l'angle pour pouvoir tourner

    def rotate(self):
        #Faire tourner le projectile une fois lancé
        self.angle += 8
        self.image = pygame.transform.rotozoom(self.origin_image, self.angle, 1)
        self.rect = self.image.get_rect(center=self.rect.center)

    def remove(self):
        self.player.all_projectiles.remove(self)

    def move(self):
        self.rect.x += self.velocity
        self.rotate()

        #Vérifier si le projectile entre en collision avec un monstre
        for monster in self.player.game.check_collision(self, self.player.game.all_monsters):
            #si c'est vrai, on supprime le projectile
            self.remove()
            #Infliger des dégats
            monster.damage(self.player.attack)

        #condition pour vérifier si le projectile en mouvement n'est plus présent sur l'écran
        if self.rect.x > 1080: 
            #Supprimer le projectile
            self.remove()